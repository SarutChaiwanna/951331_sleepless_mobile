﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagement : MonoBehaviour
{
   public GameObject m_dashboard;
   public GameObject  m_gamewin;
   public Countdown  Timx;

   public PlayerControl _isGame;
   public PlayerControl _isWin;
   
  
  public bool  isStartGame = false;
  
  // private bool isStartGame =  false;
    void Start()
    {
       isStartGame = true;
        m_dashboard.SetActive(false);
        m_gamewin.SetActive(false);
     //   isStartGame  =  true;
    }
     void Update()
    {
       
         if(_isGame.isGame)
         {
            isStartGame = false;
            m_dashboard.SetActive(true);
            Timx.totalTime = 0;
         }
         if(_isWin.isWin)
         {
            isStartGame  = false ;
            m_gamewin.SetActive(true);
            Timx.totalTime  = 0;
         }
         if(Timx.totalTime <= 0.00f)
          {
             isStartGame = false;
              m_dashboard.SetActive(true);
              Timx.totalTime  = 0;
          }

    }
}
