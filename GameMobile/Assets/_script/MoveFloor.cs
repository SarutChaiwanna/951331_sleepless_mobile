﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFloor : MonoBehaviour
{
    public  float MAX_MOVE = 2.0f;
    float _displacCounter =  0;

    [SerializeField]
    private  float _xSpeed = 2f;

    Vector3  _movementSpeed = Vector3.zero;
    void Start()
    {
        _movementSpeed.x =  _xSpeed * Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position +=  _movementSpeed;
        _displacCounter += _movementSpeed.x;
        if(Mathf.Abs(_displacCounter)>MAX_MOVE)
        {
            _displacCounter = 0;
            _movementSpeed *= -1;
        }
    }
}
