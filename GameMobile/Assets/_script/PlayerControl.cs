﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour
{
    public float  JumpForce = 1 ;
    private float m_move   = 0;
    public  Text _number;
    public Text _Hp;
    public int  _letter;
    public  int HP  = 3;

    public bool  isGame = false;
    public bool isWin = false;

    private Animator myAnimator;
    public  ButtonState _leftButton;
    public  ButtonState _rightButton;
    public ButtonState _jumpButton;
    private Rigidbody2D _rd;
 

    // Start is called before the first frame update
  public  void Start()
    {
        myAnimator = GetComponent<Animator>();
        _rd = GetComponent<Rigidbody2D>() ;      
    }

    // Update is called once per frame
   public void Update()
    {
     
    
        _number.text = _letter.ToString();
        _Hp.text= HP.ToString();

     if(!isGame && !isWin){ Vector3 theScale = transform.localScale;

        if (_leftButton._currentState == ButtonState.State.Down)
        {
            m_move = -3*Time.deltaTime;
            this.transform.Translate(m_move,0,0);
            myAnimator.SetFloat("Speed",1);
            theScale.x = -1;

        }
        if (_rightButton._currentState == ButtonState.State.Down)
        {
            m_move = 3*Time.deltaTime;
            this.transform.Translate(m_move,0,0);
            myAnimator.SetFloat("Speed",1);
              theScale.x = 1;
        }
        
        if(_jumpButton._currentState == ButtonState.State.Down  && Mathf.Abs(_rd.velocity.y) < 0.001f)
        {       
             myAnimator.SetFloat("Blend",1);
            _rd.AddForce(new  Vector2(0,  JumpForce), ForceMode2D.Impulse);
        }
        else if  (_leftButton._currentState == ButtonState.State.Up &&
        _rightButton._currentState == ButtonState.State.Up)
          {
        myAnimator.SetFloat("Speed",0);
          }

             transform.localScale = theScale;
             myAnimator.SetFloat("Blend",0);
             }
    }

  public void OnTriggerEnter2D(Collider2D other)
   {
     if(other.tag == "Item")
     {
       Destroy(other.gameObject);
       _letter--;
     }
    if(other.tag == "Trap")
     {
      HP--;
  
     }
      if (HP  <= 0)
     {
         isGame = true;
     }
     if(_letter <= 0)
     {
        isWin = true;
     }
     

    
   }
}
